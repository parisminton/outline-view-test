Outline view test
=================

Can blocks of text easily be dragged-and-dropped into place to start crafting the outline of a larger essay?

For science.

### Setup

No server necessary. Just open up the *index.html* file in a browser.
