Tinkering with Drag-and-paste
=============================

Changelog
---------

*8/16/14*

1. Pinned items are sortable.

2. Dragging a text block to the pinboard makes the pinboard its parent.


*8/15/14*

1. Dropping a text block into the target div saves its text into an array.

2. A form replaces the textarea in the third panel.


*8/14/14*

1. Dragging a div into a textarea copies text from that div.
