
$(document).ready(function() {
  var data = [];

  function parseText (elem) {
    var text = '';

    function parse (next_elem) {
      var i,
          len,
          n;

      len = next_elem.childNodes.length;

      console.log(text);
      if (len > 0) {
        for (i = 0; i < len; i += 1) {
          n = next_elem.childNodes[i];
          if (n.nodeType == 3) {
            text += n.nodeValue;
          }
          else if (n.childNodes.length > 0) {
            parse(n);
          }
          else {
            continue;
          }
        }
      }
    } // end parse

    parse(elem);
    return text += '\n\n';
  } // end parseText
 
  $('.draggable').draggable({
    revert : 'invalid',
    connectToSortable : '#pinboard_interior'
    /*  ### weirdly, snaps to #pinboard, not #pinboard_interior
    snap : '#pinboard_interior',
    snapTolerance : 50
    */
  });
  
  $('#pinboard_interior').sortable().droppable({
    drop : function (evt, dragged) {
      var item = $(dragged.draggable[0]),
          quote = item.text();

      data.push(quote);
      console.log(data);
      $('#pinboard').css('background-color', '#fff');
    },

    over : function (evt, dragged) {
      $('#pinboard').css('background-color', '#f2f2f2');
    }
  });
 
  $('.draggable').on('click', function(e) {
    if (!$(e.target).is('input') && !$(e.target).is('textarea')) {
      var div_text = $(e.target).text();
      $(e.target).data('text', div_text);
      var k = '<textarea rows="5" cols="25">' + div_text + '</textarea><br /><input type="button" value="save"><input type="button" value="cancel">';
      $(e.target).html(k);
    }
    if ($(e.target).is('input') && $(e.target).val() == 'save') {
      $(e.target).parent().html($(e.target).parent().find('textarea').val());

    } else if ($(e.target).is('input') && $(e.target).val() == 'cancel') {
      $(e.target).parent().html($(e.target).parent().data('text'));
    }
  });
});
